﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShannonFanoAlgorithm
{
    class Program
    {
        static public Dictionary<char, int> Occurances = new Dictionary<char, int>();
        static public Dictionary<int, Dictionary<char, int>> Tree = new Dictionary<int, Dictionary<char, int>>();
        static public Dictionary<char, string> EncodedCharacters = new Dictionary<char, string>();
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Default;
            Console.WriteLine("Введіть рядок, який потрібно закодувати: ");
            string str = Console.ReadLine();

            CountOccurances(str);
            ArrangeCharactersInDescendingOrder();
            Console.WriteLine();
            PrintOccurances();
            InitializeEncodedCharacters();
            BuildTree(Occurances, 0);
            Console.WriteLine();
            PrintEncodedCharacters();
            Console.WriteLine("\nЗакодований рядок:");
            EncodeString(str);
            Console.WriteLine("\n*Пробіли між символами потрібні тільки для наглядності");

            Console.ReadLine();
        }

        static void CountOccurances(string str) 
        {
            for (int i = 0; i < str.Length; i++) 
            {
                if (Occurances.Any(s => s.Key == str[i]) == true) 
                {
                    Occurances[str[i]]++;
                } 
                else 
                {
                    Occurances.Add(str[i], 1);
                }    
            }
        }

        static void PrintOccurances() 
        {
            foreach (var obj in Occurances)
            {
                Console.WriteLine(obj.Key + " - " + obj.Value);
            }
        }

        static void PrintEncodedCharacters()
        {
            foreach (var obj in EncodedCharacters)
            {
                Console.WriteLine(obj.Key + " - " + obj.Value);
            }
        }

        static void EncodeString(string str) 
        {
            foreach (char c in str) {
                Console.Write(EncodedCharacters[c] + " ");
            }
        }

        static void InitializeEncodedCharacters()
        {
            foreach (var obj in Occurances) 
            {
                EncodedCharacters.Add(obj.Key, "");
            }
        }

        static void BuildTree(Dictionary<char, int> node, int indexInTree) 
        {
            if (indexInTree == 0) Tree.Add(0, node);

            Dictionary<char, int> leftLeaf = new Dictionary<char, int>();
            Dictionary<char, int> rightLeaf = new Dictionary<char, int>();

            int minDistance = 2147483647;
            for (int i = 0; i <= node.Count - 2; i++) 
            {
                Dictionary<char, int> candidateToBeLeftLeaf = GetRangeDictionary(0, i, node);
                Dictionary<char, int> candidateToBeRightLeaf = GetRangeDictionary(i + 1, node.Count - 1, node);

                int distance = Math.Abs(
                    CountTotalOccurancesInDictionary(candidateToBeLeftLeaf) - 
                    CountTotalOccurancesInDictionary(candidateToBeRightLeaf));

                if (distance < minDistance) 
                {
                    minDistance = distance;
                    leftLeaf = candidateToBeLeftLeaf;
                    rightLeaf = candidateToBeRightLeaf;
                }
            }

            if (leftLeaf != null && rightLeaf != null && node.Count != 1) 
            {
                int leftLeafIndex = 2 * indexInTree + 1;
                int rightLeafIndex = 2 * indexInTree + 2;

                Tree.Add(leftLeafIndex, leftLeaf);
                foreach (var obj in leftLeaf)
                    EncodedCharacters[obj.Key] += "0";
                
                Tree.Add(rightLeafIndex, rightLeaf);
                foreach (var obj in rightLeaf)
                    EncodedCharacters[obj.Key] += "1";

                BuildTree(leftLeaf, leftLeafIndex);
                BuildTree(rightLeaf, rightLeafIndex);
            }
        }

        static Dictionary<char, int> GetRangeDictionary(int first, int last, Dictionary<char, int> dictionary)
        {
            Dictionary<char, int> newDictionary = new Dictionary<char, int>();

            foreach (var obj in dictionary.Select((o, index) => new { o, index })) 
            {
                if(obj.index >= first && obj.index <= last) newDictionary.Add(obj.o.Key, obj.o.Value);
            }

            return newDictionary;
        }

        static int CountTotalOccurancesInDictionary(Dictionary<char, int> dictionary) 
        {
            int sum = 0;

            foreach (var obj in dictionary) 
            {
                sum += obj.Value;
            }

            return sum;
        }

        static void ArrangeCharactersInDescendingOrder() 
        {
            Occurances = Occurances.OrderByDescending(o => o.Value).ToDictionary(o => o.Key, o => o.Value);
        }
    }
}
